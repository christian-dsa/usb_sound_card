EESchema Schematic File Version 4
LIBS:usb_sound_card-cache
EELAYER 29 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 2
Title "Project Description"
Date ""
Rev "REV100"
Comp ""
Comment1 "Status"
Comment2 "Christian Daigneault-St-Arnaud"
Comment3 "Reviewer"
Comment4 ""
$EndDescr
Wire Notes Line style solid
	1250 8200 1250 10200
Wire Notes Line style solid
	1250 10200 8350 10200
Wire Notes Line style solid
	8350 10200 8350 8200
Wire Notes Line style solid
	8350 8200 1250 8200
Text Notes 1450 8400 0    100  ~ 0
Revision
Text Notes 2650 8400 0    100  ~ 0
Date
Text Notes 4250 8400 0    100  ~ 0
Modification
Wire Notes Line style solid
	1250 8450 8350 8450
Wire Notes Line style solid
	2350 10200 2350 8200
Wire Notes Line style solid
	3300 10200 3300 8200
Wire Notes Line style solid
	1250 8750 8350 8750
Text Notes 1550 8650 0    75   ~ 0
REV100
Text Notes 3200 1900 0    250  ~ 50
Bloc Diagram
Text Notes 12400 2000 0    250  ~ 50
Notes : 
Text Notes 6400 1200 0    350  ~ 70
Project Description
$Sheet
S 3750 3550 1000 700 
U 5B4D3FA2
F0 "TOP LEVEL" 50
F1 "Top-Level.sch" 50
$EndSheet
$EndSCHEMATC
