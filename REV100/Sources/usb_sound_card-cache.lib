EESchema-LIBRARY Version 2.4
#encoding utf-8
#
# Amplifiers_TPA6132A2RTER
#
DEF Amplifiers_TPA6132A2RTER U 0 20 Y Y 1 F N
F0 "U" 0 700 60 H V C CNN
F1 "Amplifiers_TPA6132A2RTER" 0 -750 60 H V C CNN
F2 "IC:QFN-16_3x3_p0.5" 0 -1350 60 H I C CNN
F3 "" 0 -1450 60 H I C CNN
F4 "Digikey" 0 -850 60 H I C CNN "Supplier"
F5 "296-24053-1-ND" 0 -950 60 H I C CNN "Supplier Part Number"
F6 "Texas Instruments" 0 -1050 60 H I C CNN "Manufacturer"
F7 "TPA6132A2RTER" 0 -1150 60 H I C CNN "Manufacturer Part Number"
F8 "IC AMP CLASS AB STEREO 25MW 16WQFN" 0 -1250 60 H I C CNN "Description"
DRAW
S -350 650 350 -650 0 1 0 f
X INL- 1 -550 350 200 R 50 50 1 1 I
X PGND 10 550 -450 200 L 50 50 1 1 I
X CPP 11 550 -50 200 L 50 50 1 1 I
X HPVDD 12 550 250 200 L 50 50 1 1 I
X EN 13 -550 -450 200 R 50 50 1 1 I
X VDD 14 -550 550 200 R 50 50 1 1 I
X SGND 15 550 -350 200 L 50 50 1 1 I
X OUTL 16 550 450 200 L 50 50 1 1 I
X EP 17 550 -550 200 L 50 50 1 1 I
X INL+ 2 -550 250 200 R 50 50 1 1 I
X INR+ 3 -550 50 200 R 50 50 1 1 I
X INR- 4 -550 -50 200 R 50 50 1 1 I
X OUTR 5 550 550 200 L 50 50 1 1 I
X G0 6 -550 -250 200 R 50 50 1 1 I
X G1 7 -550 -350 200 R 50 50 1 1 I
X HPVSS 8 550 150 200 L 50 50 1 1 I
X CPN 9 550 -150 200 L 50 50 1 1 I
ENDDRAW
ENDDEF
#
# Capacitors_885012007053
#
DEF Capacitors_885012007053 C 0 0 N N 1 F N
F0 "C" -50 160 60 H V C CNN
F1 "Capacitors_885012007053" -50 -350 60 H I C CNN
F2 "Capacitors:C0805" -50 -950 60 H I C CNN
F3 "D" -50 -1050 60 H I C CNN
F4 "Digikey" -50 -450 60 H I C CNN "Supplier"
F5 "732-7848-1-ND" -50 -550 60 H I C CNN "Supplier Part Number"
F6 "Wurth Electronics Inc." -50 -650 60 H I C CNN "Manufacturer"
F7 "885012007053" -50 -750 60 H I C CNN "Manufacturer Part Number"
F8 "CAP CER 22PF 50V C0G/NP0 0805" -50 -850 60 H I C CNN "Description"
F9 "22pF" -50 -120 50 H V C CNN "Capacitance (Farad)"
F10 "±5%" 210 -120 50 H I C CNN "Tolerance (%)"
F11 "50V" -50 -190 50 H I C CNN "Voltage Rated (Volt)"
DRAW
P 2 0 1 0 -100 0 -90 0 N
P 2 0 1 10 -85 75 -85 -75 N
P 2 0 1 10 -15 75 -15 -75 N
P 2 0 1 0 0 0 -10 0 N
P 2 0 1 0 25 0 50 0 N
X 1 1 -200 0 100 R 50 50 1 1 P
X 2 2 100 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Capacitors_C1608NP02A101J080AA
#
DEF Capacitors_C1608NP02A101J080AA C 0 0 N N 1 F N
F0 "C" 0 160 60 H V C CNN
F1 "Capacitors_C1608NP02A101J080AA" 0 -450 60 H I C CNN
F2 "Capacitors:C0603" 0 -1050 60 H I C CNN
F3 "https://product.tdk.com/info/en/catalog/spec/mlccspec_commercial_hightemp_en.pdf" 0 -1150 60 H I C CNN
F4 "Digikey" 0 -550 60 H I C CNN "Supplier"
F5 "445-14105-1-ND" 0 -650 60 H I C CNN "Supplier Part Number"
F6 "TDK Corporation" 0 -750 60 H I C CNN "Manufacturer"
F7 "C1608NP02A101J080AA" 0 -850 60 H I C CNN "Manufacturer Part Number"
F8 "CAP CER 100PF 100V NP0 0603" 0 -950 60 H I C CNN "Description"
F9 "100pF" 0 -150 50 H V C CNN "Capacitance (Farad)"
F10 "±5%" 450 -150 50 H I C CNN "Tolerance (%)"
F11 "100V" 0 -250 50 H V C CNN "Voltage Rated (Volt)"
DRAW
P 2 0 1 0 -50 0 -40 0 N
P 2 0 1 10 -35 75 -35 -75 N
P 2 0 1 10 35 75 35 -75 N
P 2 0 1 0 50 0 40 0 N
P 2 0 1 0 75 0 100 0 N
X 1 1 -150 0 100 R 50 50 1 1 P
X 2 2 150 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Capacitors_C1608X8L1C105K080AC
#
DEF Capacitors_C1608X8L1C105K080AC C 0 0 N N 1 F N
F0 "C" 0 160 60 H V C CNN
F1 "Capacitors_C1608X8L1C105K080AC" 0 -450 60 H I C CNN
F2 "Capacitors:C0603" 0 -1050 60 H I C CNN
F3 "https://product.tdk.com/info/en/catalog/spec/mlccspec_commercial_hightemp_en.pdf" 0 -1150 60 H I C CNN
F4 "Digikey" 0 -550 60 H I C CNN "Supplier"
F5 "445-175407-1-ND" 0 -650 60 H I C CNN "Supplier Part Number"
F6 "TDK Corporation" 0 -750 60 H I C CNN "Manufacturer"
F7 "C1608X8L1C105K080AC" 0 -850 60 H I C CNN "Manufacturer Part Number"
F8 "CAP CER 1UF 16V X8L 0603" 0 -950 60 H I C CNN "Description"
F9 "1µF" 0 -150 50 H V C CNN "Capacitance (Farad)"
F10 "±10%" 450 -150 50 H I C CNN "Tolerance (%)"
F11 "16V" 0 -250 50 H V C CNN "Voltage Rated (Volt)"
DRAW
P 2 0 1 0 -50 0 -40 0 N
P 2 0 1 10 -35 75 -35 -75 N
P 2 0 1 10 35 75 35 -75 N
P 2 0 1 0 50 0 40 0 N
P 2 0 1 0 75 0 100 0 N
X 1 1 -150 0 100 R 50 50 1 1 P
X 2 2 150 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Capacitors_TAJA106K006RNJ
#
DEF Capacitors_TAJA106K006RNJ C 0 0 N N 1 F N
F0 "C" 0 150 60 H V C CNN
F1 "Capacitors_TAJA106K006RNJ" 0 -440 60 H I C CNN
F2 "Capacitors:C1206_polarized" 0 -1050 60 H I C CNN
F3 "" 0 -1150 60 H I C CNN
F4 "Digikey" 0 -550 60 H I C CNN "Supplier"
F5 "478-1653-1-ND" 0 -650 60 H I C CNN "Supplier Part Number"
F6 "AVX Corporation" 0 -750 60 H I C CNN "Manufacturer"
F7 "TAJA106K006RNJ" 0 -850 60 H I C CNN "Manufacturer Part Number"
F8 "CAP TANT 10UF 10% 6.3V 1206 4Ohm" 0 -950 60 H I C CNN "Description"
F9 "10µF" 0 -150 50 H V C CNN "Capacitance (Farad)"
F10 "±10%" 500 -150 60 H I C CNN "Tolerance (%)"
F11 "6.3V" 0 -250 50 H V C CNN "Voltage Rated (Volt)"
DRAW
A 150 0 125 -1431 1431 0 1 10 N 50 -75 50 75
P 2 0 1 0 -70 -15 -70 -65 N
P 2 0 1 0 -50 0 -25 0 N
P 2 0 1 0 -45 -40 -95 -40 N
P 2 0 1 10 -25 -75 -25 75 N
P 2 0 1 0 50 0 25 0 N
X Positif 1 -150 0 100 R 50 50 1 1 P
X Negatif 2 150 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Connectors_10118194-0001LF
#
DEF Connectors_10118194-0001LF J 0 40 Y Y 1 F N
F0 "J" 0 350 60 H V C CNN
F1 "Connectors_10118194-0001LF" 0 -550 60 H I C CNN
F2 "Connectors:10118194" 0 -1150 60 H I C CNN
F3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/10118194.pdf" 0 -1250 60 H I C CNN
F4 "Digikey" 0 -650 60 H I C CNN "Supplier"
F5 "609-4618-1-ND" 0 -750 60 H I C CNN "Supplier Part Number"
F6 "Amphenol FCI" 0 -850 60 H I C CNN "Manufacturer"
F7 "10118194-0001LF" 0 -950 60 H I C CNN "Manufacturer Part Number"
F8 "CONN USB MICRO B RECPT SMT & TH R/A" 0 -1050 60 H I C CNN "Description"
DRAW
S -350 300 350 -300 0 1 0 f
X VBUS 1 -550 200 200 R 50 50 1 1 B
X D- 2 -550 100 200 R 50 50 1 1 I
X D+ 3 -550 0 200 R 50 50 1 1 I
X ID 4 -550 -100 200 R 50 50 1 1 I
X GND 5 -550 -200 200 R 50 50 1 1 I
X SHIELD 6 550 100 200 L 50 50 1 1 I
X SHIELD 7 550 0 200 L 50 50 1 1 I
X SHIELD 8 550 -100 200 L 50 50 1 1 I
X SHIELD 9 550 -200 200 L 50 50 1 1 I
ENDDRAW
ENDDEF
#
# Connectors_61300311121
#
DEF Connectors_61300311121 J 0 40 Y N 1 F N
F0 "J" 0 200 60 H V C CNN
F1 "Connectors_61300311121" 0 -300 60 H I C CNN
F2 "Connectors:61300311121" 0 -900 60 H I C CNN
F3 "http://katalog.we-online.de/em/datasheet/6130xx11121.pdf" 0 -1000 60 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "732-5316-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Wurth Electronics Inc." 0 -600 60 H I C CNN "Manufacturer"
F7 "61300311121" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "CONN HEADER 3 POS 2.54" 0 -800 60 H I C CNN "Description"
DRAW
S 200 150 -200 -150 0 1 0 f
X 1 1 400 100 200 L 50 50 1 1 P
X 2 2 400 0 200 L 50 50 1 1 P
X 3 3 400 -100 200 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Connectors_ACJS-MHOM
#
DEF Connectors_ACJS-MHOM J 0 0 N N 1 F N
F0 "J" 0 400 60 H V C CNN
F1 "Connectors_ACJS-MHOM" 0 -500 60 H I C CNN
F2 "Connectors:ACJS-MHOM" 0 -1100 60 H I C CNN
F3 "" 0 -1200 60 H I C CNN
F4 "Digikey" 0 -600 60 H I C CNN "Supplier"
F5 "889-1818-ND" 0 -700 60 H I C CNN "Supplier Part Number"
F6 "Amphenol Sine Systems Corp" 0 -800 60 H I C CNN "Manufacturer"
F7 "ACJS-MHOM" 0 -900 60 H I C CNN "Manufacturer Part Number"
F8 "CONN JACK STEREO 1/4\" R/A" 0 -1000 60 H I C CNN "Description"
DRAW
S -350 100 -250 -200 0 1 0 N
P 3 0 1 0 150 -100 50 -100 50 -150 N
P 3 0 1 0 150 0 50 0 50 50 N
P 3 0 1 0 150 200 50 200 50 250 N
P 3 0 1 0 150 300 -300 300 -300 100 N
P 4 0 1 0 -200 100 -150 50 -100 100 150 100 N
P 4 0 1 0 -150 -200 -100 -150 -50 -200 150 -200 N
P 4 0 1 0 0 -150 100 -150 50 -200 0 -150 N
P 4 0 1 0 0 50 100 50 50 100 0 50 N
P 4 0 1 0 0 250 100 250 50 300 0 250 N
X Tip_Switch 1 350 -100 200 L 50 50 1 1 I
X Ring_Switch 2 350 0 200 L 50 50 1 1 I
X Sleeve_Switch 3 350 200 200 L 50 50 1 1 I
X Sleeve 4 350 300 200 L 50 50 1 1 I
X Ring 5 350 100 200 L 50 50 1 1 I
X Tip 6 350 -200 200 L 50 50 1 1 I
ENDDRAW
ENDDEF
#
# Connectors_Mounting_Hole_3mm
#
DEF Connectors_Mounting_Hole_3mm H 0 0 N N 1 F N
F0 "H" 0 250 60 H V C CNN
F1 "Connectors_Mounting_Hole_3mm" 0 -300 60 H I C CNN
F2 "Connectors:Mounting_Hole_3mm" 0 -900 60 H I C CNN
F3 "D" 0 -1000 60 H I C CNN
F4 "N/A" 0 -400 60 H I C CNN "Supplier"
F5 "N/A" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "N/A" 0 -600 60 H I C CNN "Manufacturer"
F7 "N/A" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "N/A" 0 -800 60 H I C CNN "Description"
DRAW
C 0 0 150 0 1 0 N
C 0 0 200 0 1 0 N
X 1 1 300 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Connectors_SJ-3524-SMT-TR-PI
#
DEF Connectors_SJ-3524-SMT-TR-PI J 0 0 N N 1 F N
F0 "J" 0 300 60 H V C CNN
F1 "Connectors_SJ-3524-SMT-TR-PI" 0 -300 60 H I C CNN
F2 "Connectors:SJ-3524-SMT-PINK" 0 -900 60 H I C CNN
F3 "https://www.cui.com/product/resource/sj-352x-smt-series.pdf" 0 -1000 60 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "CP-3524SJPICT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "CUI Inc." 0 -600 60 H I C CNN "Manufacturer"
F7 "SJ-3524-SMT-TR-PI" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "CONN JACK STEREO 3.5MM SMD R/A" 0 -800 60 H I C CNN "Description"
F9 "Pink" -300 -150 50 H V C CNN "Color"
DRAW
S -350 100 -250 -100 0 1 0 N
P 3 0 1 0 150 0 50 0 50 -50 N
P 3 0 1 0 150 200 -300 200 -300 100 N
P 4 0 1 0 -200 100 -150 50 -100 100 150 100 N
P 4 0 1 0 -150 -100 -100 -50 -50 -100 150 -100 N
P 4 0 1 0 0 -50 100 -50 50 -100 0 -50 N
X Sleeve 1 350 200 200 L 50 50 1 1 I
X Tip 2 350 -100 200 L 50 50 1 1 I
X Ring 3 350 100 200 L 50 50 1 1 I
X Tip_Switch 4 350 0 200 L 50 50 1 1 I
ENDDRAW
ENDDEF
#
# Connectors_SJ-43515RS-SMT-TR
#
DEF Connectors_SJ-43515RS-SMT-TR J 0 0 N N 1 F N
F0 "J" 0 300 60 H V C CNN
F1 "Connectors_SJ-43515RS-SMT-TR" 0 -300 60 H I C CNN
F2 "Connectors:SJ-43515RS-SMT" 0 -900 60 H I C CNN
F3 "" 0 -1000 60 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "CP-43515RSSJTR-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "CUI Inc." 0 -600 60 H I C CNN "Manufacturer"
F7 "SJ-43515RS-SMT-TR" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "CONN JACK 4COND 3.5MM SMD R/A" 0 -800 60 H I C CNN "Description"
F9 "Black" -300 -150 50 H V C CNN "Color"
DRAW
S -350 100 -250 -100 0 1 0 N
P 2 0 1 0 150 -50 150 0 N
P 3 0 1 0 150 250 -300 250 -300 100 N
P 4 0 1 0 -200 150 -150 100 -100 150 150 150 N
P 4 0 1 0 -100 50 -50 0 0 50 250 50 N
P 4 0 1 0 0 -150 50 -100 100 -150 150 -150 N
P 4 0 1 0 100 0 200 0 150 50 100 0 N
X Sleeve 1 350 250 200 L 50 50 1 1 P
X Tip 2 350 -150 200 L 50 50 1 1 P
X Ring 3 350 50 200 L 50 50 1 1 P
X Ring 4 350 150 200 L 50 50 1 1 P
X Tip_Switch 6 350 -50 200 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Crystals_Oscillators_ABLS-6.000MHZ-B4-T
#
DEF Crystals_Oscillators_ABLS-6.000MHZ-B4-T Y 0 0 N N 1 F N
F0 "Y" 0 200 60 H V C CNN
F1 "Crystals_Oscillators_ABLS-6.000MHZ-B4-T" 0 -200 60 H V C CNN
F2 "Crystals:HC49_US" 0 -900 60 H I C CNN
F3 "" 0 -1000 60 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "535-10208-1-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Abracon LLC" 0 -600 60 H I C CNN "Manufacturer"
F7 "ABLS-6.000MHZ-B4-T" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "CRYSTAL 6.0000MHZ 18PF SMD" 0 -800 60 H I C CNN "Description"
F9 "6MHz" 0 -300 50 H I C CNN "Frequency"
DRAW
S -40 120 40 -120 1 1 10 N
P 2 1 1 0 -100 0 -70 0 N
P 2 1 1 0 100 0 70 0 N
P 3 1 1 0 -70 100 -70 0 -70 -100 N
P 3 1 1 0 70 100 70 0 70 -100 N
X 1 1 -200 0 100 R 50 50 1 1 P
X 2 2 200 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Diodes_150080BS75000
#
DEF Diodes_150080BS75000 D 0 0 N N 1 F N
F0 "D" -50 150 60 H V C CNN
F1 "Diodes_150080BS75000" 0 -300 60 H I C CNN
F2 "Diodes:LED0805-BLUE" 10 -1080 60 H I C CNN
F3 "https://katalog.we-online.de/led/datasheet/150080BS75000.pdf" 10 -1180 60 H I C CNN
F4 "Digikey" 10 -580 60 H I C CNN "Supplier"
F5 "732-4982-1-ND" 10 -680 60 H I C CNN "Supplier Part Number"
F6 "Wurth Electronics Inc." 10 -780 60 H I C CNN "Manufacturer"
F7 "150080BS75000" 10 -880 60 H I C CNN "Manufacturer Part Number"
F8 "LED BLUE CLEAR 0805 SMD" 10 -980 60 H I C CNN "Description"
F9 "Blue" 0 -200 60 H V C CNN "Color"
F10 "3.2V" 0 -1250 60 H I C CNN "Voltage - Forward (Vf) (Typ)"
DRAW
P 2 0 1 15 0 -50 0 50 N
P 4 0 1 10 -100 50 -100 -50 0 0 -100 50 f
P 5 0 1 10 -85 -70 -30 -125 -40 -95 -60 -115 -30 -125 F
P 5 0 1 10 -25 -65 30 -120 0 -110 20 -90 30 -120 F
X Cathode 1 100 0 100 L 50 50 1 1 P
X Anode 2 -200 0 100 R 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Diodes_CM1231-02SO
#
DEF Diodes_CM1231-02SO D 0 40 Y Y 1 F N
F0 "D" 0 200 50 H V C CNN
F1 "Diodes_CM1231-02SO" 0 -300 60 H V C CNN
F2 "Diodes:SOT-23-6" 0 -920 60 H I C CNN
F3 "http://www.onsemi.com/pub/Collateral/CM1231%20DATA%20SHEET.PDF" 0 -1020 60 H I C CNN
F4 "Digikey" 0 -420 60 H I C CNN "Supplier"
F5 "CM1231-02SOOSCT-ND" 0 -520 60 H I C CNN "Supplier Part Number"
F6 "ON Semiconductor" 0 -620 60 H I C CNN "Manufacturer"
F7 "CM1231-02SO" 0 -720 60 H I C CNN "Manufacturer Part Number"
F8 "TVS DIODE 5V 9V SOT23-6" 0 -820 60 H I C CNN "Description"
DRAW
S -300 150 300 -250 0 1 0 f
X AOUT 1 -500 -100 200 R 50 50 1 1 P
X VN 2 -500 -200 200 R 50 50 1 1 P
X AIN 3 500 -100 200 L 50 50 1 1 P
X BIN 4 500 0 200 L 50 50 1 1 P
X VP 5 -500 100 200 R 50 50 1 1 P
X BOUT 6 -500 0 200 R 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Filters_CIS10P121AC
#
DEF Filters_CIS10P121AC FB 0 0 N N 1 F N
F0 "FB" 0 100 60 H V C CNN
F1 "Filters_CIS10P121AC" 0 -250 60 H I C CNN
F2 "Filters:FB0603" 0 -850 60 H I C CNN
F3 "http://www.samsungsem.com/kr/support/product-search/bead/__icsFiles/afieldfile/2016/05/30/CIS10P121AC.pdf" 0 -950 60 H I C CNN
F4 "Digikey" 0 -350 60 H I C CNN "Supplier"
F5 "1276-6351-1-ND" 0 -450 60 H I C CNN "Supplier Part Number"
F6 "Samsung Electro-Mechanics" 0 -550 60 H I C CNN "Manufacturer"
F7 "CIS10P121AC" 0 -650 60 H I C CNN "Manufacturer Part Number"
F8 "FERRITE BEAD 120 OHM 0603 1LN" 0 -750 60 H I C CNN "Description"
F9 "120R @ 100MHz" 0 -100 50 H V C CNN "Impedance @ Freq"
F10 "3A" -10 -170 50 H V C CNN "Current Rating (Ampere)"
DRAW
P 2 0 1 0 -170 0 -160 0 N
P 2 0 1 0 -50 0 50 0 N
P 2 0 1 0 160 0 170 0 N
P 5 0 1 0 -100 50 -100 -50 100 -50 100 50 -100 50 N
X 1 1 -200 0 100 R 50 50 1 1 P
X 2 2 200 0 100 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Fuses_1206L025YR
#
DEF Fuses_1206L025YR F 0 0 N N 1 F N
F0 "F" 0 100 60 H V C CNN
F1 "Fuses_1206L025YR" 0 -300 50 H I C CNN
F2 "Fuses:1206L025" 0 -900 30 H I C CNN
F3 "" 0 -950 30 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "F2110CT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Littelfuse Inc." 0 -600 60 H I C CNN "Manufacturer"
F7 "F2110CT-ND" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "PTC RESET FUSE 16V 250MA 1206" 0 -800 50 H I C CNN "Description"
F9 "500mA" 0 -150 50 H I C CNN "Current - Trip"
F10 "250mA" 0 -150 60 H V C CNN "Current - Hold"
DRAW
C 107 -52 5 0 0 5 N
T 0 50 -75 50 0 0 0 +t Normal 0 C C
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 3 0 1 0 -111 -75 -61 -75 64 75 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Inductors_LQM31PN4R7M00L
#
DEF Inductors_LQM31PN4R7M00L L 0 0 N N 1 F N
F0 "L" -10 160 60 H V C CNN
F1 "Inductors_LQM31PN4R7M00L" 0 -300 60 H I C CNN
F2 "Inductors:I1206" 0 -900 60 H I C CNN
F3 "" 0 -1000 60 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "490-6706-1-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Murata Electronics" 0 -600 60 H I C CNN "Manufacturer"
F7 "LQM31PN4R7M00L" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "FIXED IND 4.7UH 700MA 300 MOHM" 0 -800 60 H I C CNN "Description"
F9 "4.7µH" 0 -50 50 H V C CNN "Inductance (Henry)"
F10 "700mA" -10 -120 50 H V C CNN "Current Rating (Ampere)"
DRAW
A -120 0 40 1 1799 0 1 0 N -80 0 -160 0
A -40 0 40 1 1799 0 1 0 N 0 0 -80 0
A 40 0 40 1 1799 0 1 0 N 80 0 0 0
A 120 0 40 1 1799 0 1 0 N 160 0 80 0
P 2 0 1 0 -170 0 -160 0 N
P 2 0 1 0 -160 60 160 60 N
P 2 0 1 0 -160 80 160 80 N
P 2 0 1 0 160 0 170 0 N
X 1 1 -250 0 80 R 50 50 1 1 P
X 2 2 250 0 80 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Interface_PCM2912APJTR
#
DEF Interface_PCM2912APJTR U 0 40 Y Y 1 F N
F0 "U" 0 1450 60 H V C CNN
F1 "Interface_PCM2912APJTR" 0 -1000 60 H V C CNN
F2 "IC:QFP-32_7x7_p0.8" 0 -1600 60 H I C CNN
F3 "" 0 -1700 60 H I C CNN
F4 "Digikey" 0 -1100 60 H I C CNN "Supplier"
F5 "296-39145-1-ND" 0 -1200 60 H I C CNN "Supplier Part Number"
F6 "Texas Instruments" 0 -1300 60 H I C CNN "Manufacturer"
F7 "PCM2912APJTR" 0 -1400 60 H I C CNN "Manufacturer Part Number"
F8 "IC STEREO AUD CODEC W/USB 32TQFP" 0 -1500 60 H I C CNN "Description"
DRAW
S -450 1400 450 -950 0 1 0 f
X BGND 1 -650 -550 200 R 50 50 1 1 I
X FR 10 650 200 200 L 50 50 1 1 I
X VCOM1 11 650 50 200 L 50 50 1 1 I
X VCOM2 12 650 -50 200 L 50 50 1 1 I
X AGND 13 -650 -750 200 R 50 50 1 1 I
X NC 14 -650 850 200 R 50 50 1 1 N
X VCCA 15 650 1200 200 L 50 50 1 1 I
X VIN 16 650 700 200 L 50 50 1 1 I
X MBIAS 17 650 500 200 L 50 50 1 1 I
X VOUTL 18 650 -350 200 L 50 50 1 1 I
X VCCL 19 650 1100 200 L 50 50 1 1 I
X VBUS 2 -650 1300 200 R 50 50 1 1 I
X HGND 20 650 -550 200 L 50 50 1 1 I
X VCCR 21 650 1000 200 L 50 50 1 1 I
X VOUTR 22 650 -450 200 L 50 50 1 1 I
X MAMP 23 -650 650 200 R 50 50 1 1 I
X POWER 24 -650 550 200 R 50 50 1 1 I
X PGND 25 -650 -850 200 R 50 50 1 1 I
X VCCP 26 650 900 200 L 50 50 1 1 I
X TEST1 27 -650 -150 200 R 50 50 1 1 I
X TEST0 28 -650 -250 200 R 50 50 1 1 I
X ~SSPND 29 -650 350 200 R 50 50 1 1 I
X D- 3 -650 1150 200 R 50 50 1 1 B
X MMUTE 30 -650 450 200 R 50 50 1 1 I
X ~REC 31 650 -750 200 L 50 50 1 1 I
X ~PLAY 32 650 -850 200 L 50 50 1 1 I
X D+ 4 -650 1050 200 R 50 50 1 1 B
X VDD 5 650 1300 200 L 50 50 1 1 I
X DGND 6 -650 -650 200 R 50 50 1 1 I
X XTIO 7 -650 150 200 R 50 50 1 1 I
X XTI 8 -650 50 200 R 50 50 1 1 I
X FL 9 650 300 200 L 50 50 1 1 I
ENDDRAW
ENDDEF
#
# Power_Port_5V_USB
#
DEF Power_Port_5V_USB #PWR 0 0 N N 1 F P
F0 "#PWR" 0 -150 50 H I C CNN
F1 "Power_Port_5V_USB" -10 140 50 H V C CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 -150 60 H V C CNN
DRAW
P 2 0 1 0 -70 100 0 100 N
P 2 0 1 0 0 0 0 100 N
P 2 0 1 0 0 100 70 100 N
X 5V_USB 1 0 0 0 U 50 50 1 1 W N
ENDDRAW
ENDDEF
#
# Power_Port_Chassis
#
DEF Power_Port_Chassis #PWR 0 0 N N 1 F P
F0 "#PWR" -10 -110 50 H I C CNN
F1 "Power_Port_Chassis" 0 160 50 H I C CNN
F2 "" 0 100 60 H V C CNN
F3 "" -20 -90 60 H V C CNN
DRAW
P 2 0 1 0 -50 0 -70 -60 N
P 2 0 1 0 0 0 -20 -60 N
P 2 0 1 0 0 0 50 0 N
P 2 0 1 0 50 0 30 -60 N
P 3 0 1 0 0 100 0 0 -50 0 N
X Chassis 1 0 100 0 U 50 50 1 1 W N
ENDDRAW
ENDDEF
#
# Power_Port_GND
#
DEF Power_Port_GND #PWR 0 0 N N 1 F P
F0 "#PWR" 0 -170 50 H I C CNN
F1 "Power_Port_GND" 0 -100 50 H V C CNN
F2 "" 0 100 60 H V C CNN
F3 "" -20 -90 60 H V C CNN
DRAW
P 2 0 1 0 0 0 50 0 N
P 3 0 1 0 -50 0 0 -60 50 0 N
P 3 0 1 0 0 100 0 0 -50 0 N
X GND 1 0 100 0 U 50 50 1 1 W N
ENDDRAW
ENDDEF
#
# Power_Port_PWR_FLAG
#
DEF Power_Port_PWR_FLAG #FLG 0 0 N N 1 F P
F0 "#FLG" 0 -100 60 H I C CNN
F1 "Power_Port_PWR_FLAG" 0 200 30 H V C CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 0 60 H V C CNN
DRAW
P 6 0 1 0 0 0 0 50 -50 100 0 150 50 100 0 50 N
X pwr 1 0 0 0 U 50 50 0 0 w
ENDDRAW
ENDDEF
#
# Power_Port_SGND
#
DEF Power_Port_SGND #PWR 0 0 N N 1 F P
F0 "#PWR" 0 -170 50 H I C CNN
F1 "Power_Port_SGND" 0 -100 50 H V C CNN
F2 "" 0 100 60 H V C CNN
F3 "" -20 -90 60 H V C CNN
DRAW
P 2 0 1 0 0 0 50 0 N
P 3 0 1 0 -50 0 0 -60 50 0 N
P 3 0 1 0 0 100 0 0 -50 0 N
X SGND 1 0 100 0 U 50 50 1 1 W N
ENDDRAW
ENDDEF
#
# Power_Port_VDD
#
DEF Power_Port_VDD #PWR 0 0 N N 1 F P
F0 "#PWR" 0 -150 50 H I C CNN
F1 "Power_Port_VDD" -10 140 50 H V C CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 -150 60 H V C CNN
DRAW
P 2 0 1 0 -70 100 0 100 N
P 2 0 1 0 0 0 0 100 N
P 2 0 1 0 0 100 70 100 N
X VDD 1 0 0 0 U 50 50 1 1 W N
ENDDRAW
ENDDEF
#
# Resistors_RC0805JR-071KL
#
DEF Resistors_RC0805JR-071KL R 0 0 N N 1 F N
F0 "R" 0 80 60 H V C CNN
F1 "Resistors_RC0805JR-071KL" 0 -300 50 H I C CNN
F2 "Resistors:R0805" 0 -900 30 H I C CNN
F3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_7.pdf" 0 -950 30 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "311-1.0KARCT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Yageo" 0 -600 60 H I C CNN "Manufacturer"
F7 "RC0805JR-071KL" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "RES SMD 1K OHM 5% 1/8W 0805" 0 -800 60 H I C CNN "Description"
F9 "1k" 0 -70 50 H V C CNN "Resistance (Ohms)"
F10 "±5%" 410 -80 50 H I C CNN "Tolerance (%)"
F11 "1/8W" 0 -140 50 H I C CNN "Power (Watts)"
DRAW
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Resistors_RC0805JR-071ML
#
DEF Resistors_RC0805JR-071ML R 0 0 N N 1 F N
F0 "R" 0 80 60 H V C CNN
F1 "Resistors_RC0805JR-071ML" 0 -300 50 H I C CNN
F2 "Resistors:R0805" 0 -900 30 H I C CNN
F3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_7.pdf" 0 -950 30 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "311-1.0MARCT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Yageo" 0 -600 60 H I C CNN "Manufacturer"
F7 "RC0805JR-071ML" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "RES SMD 1M OHM 5% 1/8W 0805" 0 -800 60 H I C CNN "Description"
F9 "1M" 0 -70 50 H V C CNN "Resistance (Ohms)"
F10 "±5%" 410 -80 50 H I C CNN "Tolerance (%)"
F11 "1/8W" 0 -140 50 H I C CNN "Power (Watts)"
DRAW
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Resistors_RC0805JR-072K2L
#
DEF Resistors_RC0805JR-072K2L R 0 0 N N 1 F N
F0 "R" 0 80 60 H V C CNN
F1 "Resistors_RC0805JR-072K2L" 0 -300 50 H I C CNN
F2 "Resistors:R0805" 0 -900 30 H I C CNN
F3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_7.pdf" 0 -950 30 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "311-2.2KARCT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Yageo" 0 -600 60 H I C CNN "Manufacturer"
F7 "RC0805JR-072K2L" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "RES SMD 2.2K OHM 5% 1/8W 0805" 0 -800 60 H I C CNN "Description"
F9 "2.2k" 0 -70 50 H V C CNN "Resistance (Ohms)"
F10 "±5%" 410 -80 50 H I C CNN "Tolerance (%)"
F11 "1/8W" 0 -140 50 H I C CNN "Power (Watts)"
DRAW
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Resistors_RC1206JR-070RL
#
DEF Resistors_RC1206JR-070RL R 0 0 N N 1 F N
F0 "R" 0 80 60 H V C CNN
F1 "Resistors_RC1206JR-070RL" 0 -300 50 H I C CNN
F2 "Resistors:R1206" 0 -900 30 H I C CNN
F3 "D" 0 -950 30 H I C CNN
F4 "Digikey" 0 -400 60 H I C CNN "Supplier"
F5 "311-0.0ERCT-ND" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Yageo" 0 -600 60 H I C CNN "Manufacturer"
F7 "RC1206JR-070RL" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "RES SMD 0 OHM JUMPER 1/4W 1206" 0 -800 60 H I C CNN "Description"
F9 "0" 0 -70 50 H V C CNN "Resistance (Ohms)"
F10 "0" 410 -80 50 H I C CNN "Tolerance (%)"
F11 "1/4W" 0 -140 50 H I C CNN "Power (Watts)"
DRAW
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
# Resistors_Resistor
#
DEF Resistors_Resistor R 0 0 N N 1 F N
F0 "R" 0 80 60 H V C CNN
F1 "Resistors_Resistor" 0 -300 50 H I C CNN
F2 "F" 0 -900 30 H I C CNN
F3 "D" 0 -950 30 H I C CNN
F4 "Supplier" 0 -400 60 H I C CNN "Supplier"
F5 "Supplier Part Number" 0 -500 60 H I C CNN "Supplier Part Number"
F6 "Manufacturer" 0 -600 60 H I C CNN "Manufacturer"
F7 "Manufacturer Part Number" 0 -700 60 H I C CNN "Manufacturer Part Number"
F8 "Description" 0 -800 60 H I C CNN "Description"
F9 "Resistance" 0 -70 50 H V C CNN "Resistance (Ohms)"
F10 "Tolerance" 410 -80 50 H V C CNN "Tolerance (%)"
F11 "Power" 0 -140 50 H V C CNN "Power (Watts)"
DRAW
S -120 0 -150 0 0 1 0 N
S 150 0 120 0 0 1 0 N
P 13 0 1 0 -120 0 -100 30 -80 0 -60 -30 -40 0 -20 30 0 0 20 -30 40 0 60 30 80 0 100 -30 120 0 N
X 1 1 -200 0 50 R 50 50 1 1 P
X 2 2 200 0 50 L 50 50 1 1 P
ENDDRAW
ENDDEF
#
#End Library
